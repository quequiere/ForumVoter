package com.quequiere.forumvoter.command;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.LiteralText.Builder;
import org.spongepowered.api.text.action.ClickAction.OpenUrl;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.quequiere.forumvoter.ConfigSave;
import com.quequiere.forumvoter.ForumVoter;
import com.quequiere.forumvoter.PluginInfo;
import com.quequiere.forumvoter.VoteAction;

public class ForumVoteCommand implements CommandCallable
{

	@Override
	public CommandResult process(CommandSource src, String arg) throws CommandException
	{

		if (!(src instanceof Player))
		{
			return CommandResult.empty();
		}

		String args[] = arg.split(" ");

		Player p = (Player) src;

/*		String perm = "city.cityadmin";
		if (!ForumVoter.hasPerm(p, perm))
		{
			ForumVoter.sendMessage("You need the " + perm + " node to do that !", TextColors.RED, p);
			return CommandResult.success();
		}*/

	

		if (args.length == 0 || args.length == 1 && args[0].equals(""))
		{
			displayHelp(p);
		}
		else
		{
			SubCommand subc = null;

			try
			{
				subc = SubCommand.valueOf(args[0]);
			}
			catch (IllegalArgumentException e)
			{
				displayHelp(p);
				return CommandResult.success();
			}

			if (subc.equals(SubCommand.help))
			{
				displayHelp(p);
			}
			else if (subc.equals(SubCommand.vote))
			{
				boolean b = VoteAction.canBeReward(p.getName(), System.currentTimeMillis());
				if(b)
				{
					ForumVoter.sendMessageWithoutPrefix("---------------------------------------",TextColors.GREEN, p);
					ForumVoter.sendMessageWithoutPrefix("You can let a message on the pixelmonMod forum to win a reward !",TextColors.GREEN, p);
					ForumVoter.sendMessageWithoutPrefix("The reply need to have more than 30 characters",TextColors.GREEN, p);
					ForumVoter.sendMessageWithoutPrefix("You need to reply with the same nickname than ingame nickname.",TextColors.GREEN, p);
					
					Builder builder = Text.builder("");
					try
					{
						OpenUrl ur = TextActions.openUrl(new URL("http://pixelmonmod.com/viewtopic.php?f=125&t=24578"));
						builder.append(Text.of(TextColors.GREEN,"Link :",TextColors.GOLD,ur," here"));
					}
					catch (MalformedURLException e)
					{
						e.printStackTrace();
					}
					
					p.sendMessage(builder.build());
					ForumVoter.sendMessageWithoutPrefix("Once finished, use /fv verifyvote",TextColors.GREEN, p);
					ForumVoter.sendMessageWithoutPrefix("---------------------------------------",TextColors.GREEN, p);
				}
				else
				{
					int totalSecs=(int) VoteAction.getRemainingTime(p.getName(), System.currentTimeMillis());
					totalSecs/=1000;
					int hours = totalSecs / 3600;
					int minutes = (totalSecs % 3600) / 60;
					int seconds = totalSecs % 60;
					ForumVoter.sendMessage("You need to wait "+hours+":"+minutes+":"+seconds+" for the next forum post !", TextColors.RED, p);
				}
			}
			else if (subc.equals(SubCommand.verifyvote))
			{
				
				long last = ConfigSave.getConfig().lastStart;
				long now = System.currentTimeMillis();
				long diff = now-last;
				
				if(diff<3*60*1000)
				{
					ForumVoter.sendMessage("The server need to wait 3 minutes between two check, please try latter", TextColors.RED, p);
					return CommandResult.success();
				}
				else if(ConfigSave.getConfig().isrunning)
				{
					ForumVoter.sendMessage("Aa check is currently running, please wait.", TextColors.RED, p);
					return CommandResult.success();
				}
				
				
				Task.Builder taskBuilder = Task.builder();
				taskBuilder.execute(
					    task -> {
					    	try {
					    		ConfigSave.getConfig().lastStart=System.currentTimeMillis();
					    		ConfigSave.getConfig().isrunning=true;
					    		ConfigSave.getConfig().save();
								VoteAction.askUpdate(ConfigSave.getConfig().lastPage, ConfigSave.getConfig().lastCheckedDate);
							}
					    	finally{
								ConfigSave.getConfig().isrunning=false;
								ConfigSave.getConfig().needUpdate=true;
					    		ConfigSave.getConfig().save();
					    	}
					    }
					).async().name("ForumVoter load web thread").submit(ForumVoter.plugin);
				
				
				ForumVoter.sendMessage("Verification on pixelmonmod server. It could take few min.", TextColors.GREEN, p);
			}
			else
			{
				ForumVoter.sendMessage("Dev error, you shouldn't be here !", TextColors.RED, p);
			}

		}

		return CommandResult.success();

	}

	public enum SubCommand
	{
		help, vote,verifyvote
	};

	public static void displayHelp(Player p)
	{

		ForumVoter.sendMessage("City plugin " + PluginInfo.VERSION, TextColors.GREEN, p);
		ForumVoter.sendMessage("List of possibilities: ", TextColors.RED, p);
		for (SubCommand sc : SubCommand.values())
		{
			ForumVoter.sendMessage("/fv " + sc.name(), TextColors.RED, p);
		}
	}

	@Override
	public List<String> getSuggestions(CommandSource arg0, String arg1, Location<World> arg2) throws CommandException
	{
		return null;
	}

	@Override
	public boolean testPermission(CommandSource arg0)
	{
		return false;
	}

	@Override
	public Optional<Text> getShortDescription(CommandSource arg0)
	{
		return null;
	}

	@Override
	public Optional<Text> getHelp(CommandSource arg0)
	{
		return null;
	}

	@Override
	public Text getUsage(CommandSource arg0)
	{
		return null;
	}

}
