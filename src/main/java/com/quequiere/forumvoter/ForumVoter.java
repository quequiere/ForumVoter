package com.quequiere.forumvoter;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColor;
import com.quequiere.forumvoter.command.ForumVoteCommand;


@Plugin(id = PluginInfo.ID, name = PluginInfo.NAME, version = PluginInfo.VERSION, description = PluginInfo.DESCRIPTION, authors = { "quequiere" })
public class ForumVoter
{
	
	public static ForumVoter plugin;

	@Listener
	public void preInit(GamePreInitializationEvent e)
	{
		plugin = this;
		CommandManager cmdService = Sponge.getCommandManager();
		cmdService.register(this, new ForumVoteCommand(), "forumvote", "fv");
		
		Task.Builder taskBuilder = Task.builder();
		taskBuilder.execute(
			    task -> {
			        if(ConfigSave.getConfig().needUpdate)
			        {
			        	ConfigSave.getConfig().needUpdate=false;
			        	ArrayList<String> liste = (ArrayList<String>) VoteAction.toReward.clone();
			        	for(String name:liste)
			        	{
			        		VoteAction.rewardPlayer(name);
			        	}
			        	
			        	VoteAction.toReward.clear();
			        }
			    }
			).interval(10, TimeUnit.SECONDS).name("ForumVoterSynchronizer").submit(plugin);;
		
	}
	
	
	public static void sendMessageWithoutPrefix(String message, TextColor color, Player p)
	{
		p.sendMessage(Text.of(color, message));
	}

	public static void sendMessage(String message, TextColor color, Player p)
	{
		p.sendMessage(Text.of(color, "[ForumVote] " + message));
	}

	public static void sendMessage(String message, TextColor color, Object desc, Player p)
	{
		p.sendMessage(Text.of(color, TextActions.showText(Text.of(desc)), "[ForumVote] " + message));
	}

	public static boolean hasPerm(Player p, String perm)
	{
		if (p.hasPermission(perm))
			return true;

		return false;
	}



}
