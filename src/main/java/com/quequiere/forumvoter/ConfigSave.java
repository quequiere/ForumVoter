package com.quequiere.forumvoter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ConfigSave
{
	private static File folder = new File("./config/forumvoter/");

	private static ConfigSave instance;
	
	public int lastPage = 0;
	public long lastCheckedDate = 1487779879000l;
	public long timeBeforeVoteAgain = 3*60*60*1000;
	private HashMap<String, Long> map = new HashMap<String, Long>();
	
	public long lastStart = 0;
	public transient boolean isrunning = false;
	public transient boolean needUpdate = false;

	
	public static ConfigSave getConfig()
	{
		if(instance==null)
		{
			instance=loadConfigFile();
		}
		return instance;
	}
	
	

	public HashMap<String, Long> getMap()
	{
		if(map==null)
		{
			map=new HashMap<String, Long>();
		}
		return map;
	}



	private static ConfigSave loadConfigFile() {
		if (!folder.exists()) {
			folder.mkdirs();
		}

		File f = new File(folder.getAbsolutePath() + "/" + "config" + ".json");
		ConfigSave c = null;
		if (f.exists()) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();
				while (line != null) {
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				c = fromJson(everything);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

		if(c==null)
		{
			System.out.println("[ForumVoter] Created a new config!");
			c = new ConfigSave();
			c.save();
		}

		c.save();

		return c;
	}

	public void save() {

		BufferedWriter writer = null;
		try {

			if (!folder.exists()) {
				folder.mkdirs();
			}

			File file = new File(folder.getAbsolutePath() + "/" + "config" + ".json");
			if (!file.exists()) {
				file.createNewFile();
			}
			writer = new BufferedWriter(new FileWriter(file));

			writer.write(this.toJson());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}

	}

	public String toJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting()
				.create();
		return gson.toJson(this);
	}

	private static ConfigSave fromJson(String s) {
		Gson gson = new GsonBuilder().create();
		return gson.fromJson(s, ConfigSave.class);
	}
	
}
