package com.quequiere.forumvoter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Inet4Address;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.NamedCause;
import org.spongepowered.api.service.economy.account.UniqueAccount;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.format.TextColors;

import io.github.hsyyid.votifierlistener.VotifierListenerPlugin;
import io.github.hsyyid.votifierlistener.utils.Utils;

public class VoteAction
{

	public static ArrayList<String> toReward = new ArrayList<String>();

	public static void main(String... args)
	{
		askUpdate(0,0);
	}

	public static void askUpdate(int lastPage, long lastChecked)
	{
		try
		{
			String myadress = Inet4Address.getLocalHost().getHostAddress();
			Socket socket = new Socket(myadress, 7878);
			PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			

			out.println(lastPage);
			out.println(lastChecked);
			out.flush();
			
			String reponse = in.readLine();
			System.out.println("Receive response:"+reponse);
			
			String[] cat = reponse.split("!#!");
			
			int lastPagerep = Integer.parseInt(cat[0]);;			long lastDaterep = Long.parseLong(cat[1]);
			String[] plist= cat[2].split("!@!");
			String[] dlist= cat[3].split("!@!");
			
			if(plist.length>0)
			{
				int x=0;
				for(String pname : plist)
				{
					if(canBeReward(pname,Long.parseLong(dlist[x])))
					{
						toReward.add(pname);
					}
					
					x++;
				}
			}
			else
			{
				System.out.println("No new vote finded !");
			}
			
			
			ConfigSave.getConfig().lastPage=lastPagerep;
			ConfigSave.getConfig().lastCheckedDate=lastDaterep;
				
			

		
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			System.out.println("No new vote ");
		}
		catch (UnknownHostException e)
		{
			System.out.println("Host problem ");
		}
		catch (IOException e)
		{
			System.out.println("Pas de connection IO.");
		}
	}


	public static boolean canBeReward(String pname, long messageDate)
	{
		HashMap<String, Long> map = ConfigSave.getConfig().getMap();
		if (!map.containsKey(pname))
		{
			return true;
		}
		else
		{
		
			if (getDiffTime(pname,messageDate) >= ConfigSave.getConfig().timeBeforeVoteAgain)
			{
				return true;
			}
			else
			{
				System.out.println(pname + " need wait to vote !");
				return false;
			}
		}
	}
	
	public static long getDiffTime(String pname, long messageDate)
	{
		HashMap<String, Long> map = ConfigSave.getConfig().getMap();
		
		long last = map.get(pname);
		long diff = messageDate - last;
		return diff;
	}
	
	public static long getRemainingTime(String pname, long messageDate)
	{
		long diff= ConfigSave.getConfig().timeBeforeVoteAgain - getDiffTime(pname,messageDate) ;
		return diff;
	}

	public static void rewardPlayer(String pname)
	{
		boolean finded = false;
		System.out.println("Rewarding: " + pname);
		for (Player p : Sponge.getServer().getOnlinePlayers())
		{
			if (p.getName().equalsIgnoreCase(pname))
			{
				finded = true;
				break;
			}
			
			
			String nameSpeciaCase = p.getName();
			nameSpeciaCase = nameSpeciaCase.replace("-", "");
			nameSpeciaCase = nameSpeciaCase.replace("_", "");
			
			if (nameSpeciaCase.equalsIgnoreCase(pname))
			{
				finded = true;
				pname = nameSpeciaCase;
				break;
			}
			
			
		}

		if (!finded)
		{
			System.out.println("Can't reward " + pname + " not connected or non existing !");
			return;
		}

		HashMap<String, Long> map = ConfigSave.getConfig().getMap();
		if (map.containsValue(map))
		{
			map.replace(pname, System.currentTimeMillis());
		}
		else
		{
			map.put(pname, System.currentTimeMillis());
		}
		
		rewritedVoteListenerEvent(pname);
		rewritedVoteListenerEvent(pname);

	}

	public static void rewritedVoteListenerEvent(String username)
	{

		MessageChannel.TO_ALL.send(Text.of(TextColors.GREEN, "[ForumVote]: ", TextColors.GOLD, username + " just post a message on pixelmon forum and got a reward! You can too with ", TextColors.DARK_PURPLE, "/fv vote"));

		for (Player player : Sponge.getServer().getOnlinePlayers())
		{
			if (player.getName().equalsIgnoreCase(username))
			{
				Random rand = new Random();
				player.sendMessage(Text.of(TextColors.GREEN, "Thanks for Voting! Here is a reward!"));

				if (Utils.isEconomyEnabled())
				{
					UniqueAccount uniqueAccount = VotifierListenerPlugin.economyService.getOrCreateAccount(player.getUniqueId()).get();
					BigDecimal decimal;

					if (Utils.getMaxMoneyReward() != Utils.getMinimumMoneyReward())
					{
						decimal = new BigDecimal(rand.nextInt(Utils.getMaxMoneyReward() - Utils.getMinimumMoneyReward()) + Utils.getMinimumMoneyReward());
					}
					else
					{
						decimal = new BigDecimal(Utils.getMinimumMoneyReward());
					}

					uniqueAccount.deposit(VotifierListenerPlugin.economyService.getDefaultCurrency(), decimal, Cause.of(NamedCause.notifier(player)));
				}

				if (!Utils.shouldGiveAllRewards())
				{
					for (int counter = 0; counter < Utils.getAmtOfRewards(); counter++)
					{
						String command = Utils.getRewards().get(rand.nextInt(Utils.getRewards().size())).replaceAll("@p", player.getName());
						Sponge.getScheduler().createTaskBuilder().execute(() -> Sponge.getCommandManager().process(Sponge.getServer().getConsole().getCommandSource().get(), command)).submit(VotifierListenerPlugin.getVotifierListenerPlugin());
					}
				}
				else
				{
					for (int counter = 0; counter < Utils.getRewards().size(); counter++)
					{
						String command = Utils.getRewards().get(counter).replaceAll("@p", player.getName());
						Sponge.getScheduler().createTaskBuilder().execute(() -> Sponge.getCommandManager().process(Sponge.getServer().getConsole().getCommandSource().get(), command)).submit(VotifierListenerPlugin.getVotifierListenerPlugin());
					}
				}

				break;
			}
		}
	}

}
